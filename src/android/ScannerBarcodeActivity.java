package ionic.qrcode.scanner;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class ScannerBarcodeActivity extends AppCompatActivity {

	SurfaceView surfaceView;
	TextView textViewBarCodeValue;
	private BarcodeDetector barcodeDetector;
	private boolean cameraReleased = false;
	private CameraSource cameraSource;
	private static final int REQUEST_CAMERA_PERMISSION = 201;
	String intentData = "";
	private static final String TAG = "QR_CODE_SCANNER:SCANNER_BARCODE_ACTIVITY";

	private void copyToClipBoard(String text){
		ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("QR code Scanner", text);
		clipboard.setPrimaryClip(clip);
	}

	private void initComponents() {
		Log.d(TAG,"initComponents 01");
		int idTBV = this.getResources().getIdentifier("txtBarcodeValue", "id", this.getPackageName());
		int idSV = this.getResources().getIdentifier("surfaceView", "id", this.getPackageName());
		textViewBarCodeValue = findViewById(idTBV);
		surfaceView = findViewById(idSV);
	}

	private void initDetectorsAndSources() {
		// Context globalContext = this.getApplicationContext();
		Log.d(TAG,"initDetectorsAndSources 01");
		Context context = ScannerBarcodeActivity.this;
		Log.d(TAG,"initDetectorsAndSources 02");
		DisplayMetrics metrics = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
		Log.d(TAG,"initDetectorsAndSources 03");
		int width = metrics.widthPixels;
		int height = metrics.heightPixels;
		Log.d(TAG,"initDetectorsAndSources... metrics: "+Integer.toString(height)+" x "+Integer.toString(width));
		Toast.makeText(context, "Barcode scanner started", Toast.LENGTH_SHORT).show();
		barcodeDetector = new BarcodeDetector.Builder(this)
				.setBarcodeFormats(Barcode.ALL_FORMATS)
				.build();
		Log.d(TAG,"initDetectorsAndSources 04");
		cameraSource = new CameraSource.Builder(this, barcodeDetector)
				// .setRequestedPreviewSize(height, width)
				.setAutoFocusEnabled(true) //you should add this feature
				.build();
		Log.d(TAG,"initDetectorsAndSources 05");	
		surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				Log.d(TAG,"surfaceView surfaceCreated");
				openCamera();
			}
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				Log.d(TAG,"surfaceView surfaceChanged");
			}
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				Log.d(TAG,"surfaceView surfaceDestroyed");
				cameraSource.stop();
			}
		});
		Log.d(TAG,"initDetectorsAndSources 06");

		barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
			@Override
			public void receiveDetections(Detector.Detections<Barcode> detections) {
				Log.d(TAG,"barcodeDetector received Detections: ");
				final SparseArray<Barcode> barCode = detections.getDetectedItems();
				if (barCode.size() > 0) {
					setBarCode(barCode);
				}
			}
			@Override
			public void release() {
				Log.d(TAG,"barcodeDetector release!");
				Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG,"onCreate");
		super.onCreate(savedInstanceState);
		int idASB = this.getResources().getIdentifier("activity_scanner_barcode", "layout", this.getPackageName());
		setContentView(idASB);
		initComponents();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (cameraReleased != true) {
			try {
				cameraSource.release();
				cameraReleased = true;
			} catch (NullPointerException ignored) {
				cameraSource = null;
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		Log.d(TAG,"onRequestPermissionsResult");
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(requestCode == REQUEST_CAMERA_PERMISSION && grantResults.length>0){
			if (grantResults[0] == PackageManager.PERMISSION_DENIED){
				finish();
			}else{
				Log.d(TAG,"onRequestPermissionsResult about to openCamera");
				openCamera();
			}
		}else{
			finish();
		}
	}

	@Override
	protected void onResume() {
		Log.d(TAG,"onResume");
		super.onResume();
		initDetectorsAndSources();
	}

	private void openCamera(){
		Log.d(TAG,"openCamera 01");
		try {
			if (ActivityCompat.checkSelfPermission(ScannerBarcodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
				Log.d(TAG,"openCamera 02");
				cameraSource.start(surfaceView.getHolder());
				Log.d(TAG,"openCamera 03");
			} else {
				Log.d(TAG,"openCamera requestpermissions");
				ActivityCompat.requestPermissions(ScannerBarcodeActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setBarCode(final SparseArray<Barcode> barCode){
		Log.d(TAG,"setBarCode 01");
		textViewBarCodeValue.post(new Runnable() {
			@Override
			public void run() {
				Log.d(TAG,"setBarCode 02");
				intentData = barCode.valueAt(0).displayValue;
				Log.d(TAG,"setBarCode intentData: "+intentData.toString());
				textViewBarCodeValue.setText(intentData);
				Log.d(TAG,"setBarCode 03");
				copyToClipBoard(intentData);
				Log.d(TAG,"setBarCode 04");
			}
		});
	}
}