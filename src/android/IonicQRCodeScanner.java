package ionic.qrcode.scanner;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IonicQRCodeScanner extends CordovaPlugin {
    private CallbackContext callback;
    public static final int INTENT_START = 1;
	private String TAG = "IonicQRCodeScanner";

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Context context = cordova.getActivity().getApplicationContext();
        callback = callbackContext;
        if(action.equals("start")){
            Log.d(TAG,"IonicQRCodeScanner execute... context.getPackageName(): "+context.getPackageName());
            Intent intent = new Intent(context, ScannerBarcodeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra("action", IonicQRCodeScanner.INTENT_START);
			cordova.startActivityForResult((CordovaPlugin) this, intent, IonicQRCodeScanner.INTENT_START);
			// startActivity(new Intent(cordova.getActivity(),ScannerBarcodeActivity.class));
            return true;
        }
        return false;
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
        switch(requestCode){
            case IonicQRCodeScanner.INTENT_START:
                Log.d(TAG,"onActivityResult requestCode: "+Integer.toString(requestCode));
                Log.d(TAG,"onActivityResult resultCode: "+Integer.toString(resultCode));
                break;
        }
    }
}