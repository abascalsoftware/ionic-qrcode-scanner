var exec = require('cordova/exec');

var service = "IonicQRCodeScanner";

exports.start = function (success, error) {
    exec(success, error, service, 'start');
};

exports.teste = function (success, error, arg0, arg1) {
	console.log("arg0: ",arg0);
	console.log("arg1: ",arg1);
	console.log("success: ",success);
	console.log("error: ",error);
    exec(success, error, service, 'teste', [arg0, arg1]);
};
