import { IonicNativePlugin } from '@ionic-native/core';
/**
 * @name IonicQRCodeScanner
 * @description
 * This plugin does something
 *
 * @usage
 * ```typescript
 * import { IonicQRCodeScanner } from '@ionic-native/ionic-qrcode-scanner';
 *
 *
 * constructor(private qrcode: IonicQRCodeScanner) { }
 *
 * ...
 *
 *
 * this.qrcode.start()
 *   .then(() => console.log("START OK"))
 *   .catch((error: any) => console.error(error));
 *
 * ```
 */
export declare class IonicQRCodeScanner extends IonicNativePlugin {
    /**
     * Start the SMS listener
     * @return {Promise<any>} Returns a promise that resolves when the SMSRetriever starts listening with success.
     */
    start(): Promise<any>;
    /**
     * This function does something
     * @param arg1 {string} Some param to configure something
     * @param arg2 {number} Another param to configure something
     * @return {Promise<any>} Returns a promise that resolves when something happens
     */
    teste(arg1: number, arg2: any): Promise<any>;
}
