var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
import { IonicNativePlugin, cordova } from '@ionic-native/core';
var IonicQRCodeScanner = /** @class */ (function (_super) {
    __extends(IonicQRCodeScanner, _super);
    function IonicQRCodeScanner() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IonicQRCodeScanner.prototype.start = function () { return cordova(this, "start", { "callbackOrder": "reverse" }, arguments); };
    IonicQRCodeScanner.prototype.teste = function (arg1, arg2) { return cordova(this, "teste", { "callbackOrder": "reverse" }, arguments); };
    IonicQRCodeScanner.pluginName = "IonicQRCodeScanner";
    IonicQRCodeScanner.plugin = "ionic-qrcode-scanner";
    IonicQRCodeScanner.pluginRef = "IonicQRCodeScanner";
    IonicQRCodeScanner.repo = "https://AndreAbascal@bitbucket.org/abascalsoftware/ionic-qrcode-scanner.git";
    IonicQRCodeScanner.platforms = ["Android"];
    IonicQRCodeScanner = __decorate([
        Injectable()
    ], IonicQRCodeScanner);
    return IonicQRCodeScanner;
}(IonicNativePlugin));
export { IonicQRCodeScanner };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2lvbmljLXNtcy1yZXRyaWV2ZXIvbmd4L2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sOEJBQTBGLE1BQU0sb0JBQW9CLENBQUM7O0lBZ0NyRixxQ0FBaUI7Ozs7SUFPdkQsaUNBQUs7SUFXTCxpQ0FBSyxhQUFDLElBQVksRUFBRSxJQUFTOzs7Ozs7SUFsQmpCLGlCQUFpQjtRQUQ3QixVQUFVLEVBQUU7T0FDQSxpQkFBaUI7NEJBakM5QjtFQWlDdUMsaUJBQWlCO1NBQTNDLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBsdWdpbiwgQ29yZG92YSwgQ29yZG92YVByb3BlcnR5LCBDb3Jkb3ZhSW5zdGFuY2UsIEluc3RhbmNlUHJvcGVydHksIElvbmljTmF0aXZlUGx1Z2luIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuLyoqXG4gKiBAbmFtZSBJb25pY1NNU1JldHJpZXZlclxuICogQGRlc2NyaXB0aW9uXG4gKiBUaGlzIHBsdWdpbiBkb2VzIHNvbWV0aGluZ1xuICpcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgSW9uaWNTTVNSZXRyaWV2ZXIgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2lvbmljLXYtNC1uYXRpdmUtc21zLXJldHJpZXZlcic7XG4gKlxuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgc21zOiBJb25pY1NNU1JldHJpZXZlcikgeyB9XG4gKlxuICogLi4uXG4gKlxuICpcbiAqIHRoaXMuc21zLnN0YXJ0KClcbiAqICAgLnRoZW4oKCkgPT4gY29uc29sZS5sb2coXCJTVEFSVCBPS1wiKSlcbiAqICAgLmNhdGNoKChlcnJvcjogYW55KSA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gKlxuICogYGBgXG4gKi9cbkBQbHVnaW4oe1xuXHRwbHVnaW5OYW1lOiAnSW9uaWNTTVNSZXRyaWV2ZXInLFxuXHRwbHVnaW46ICdpb25pYy1zbXMtcmV0cmlldmVyJywgLy8gbnBtIHBhY2thZ2UgbmFtZSwgZXhhbXBsZTogY29yZG92YS1wbHVnaW4tY2FtZXJhXG5cdHBsdWdpblJlZjogJ0lvbmljU01TUmV0cmlldmVyJywgLy8gdGhlIHZhcmlhYmxlIHJlZmVyZW5jZSB0byBjYWxsIHRoZSBwbHVnaW4sIGV4YW1wbGU6IG5hdmlnYXRvci5nZW9sb2NhdGlvblxuXHRyZXBvOiAnaHR0cHM6Ly9BbmRyZUFiYXNjYWxAYml0YnVja2V0Lm9yZy9hYmFzY2Fsc29mdHdhcmUvaW9uaWMtc21zLXJldHJpZXZlci5naXQnLCAvLyB0aGUgZ2l0aHViIHJlcG9zaXRvcnkgVVJMIGZvciB0aGUgcGx1Z2luXG5cdHBsYXRmb3JtczogWydBbmRyb2lkJ10gLy8gQXJyYXkgb2YgcGxhdGZvcm1zIHN1cHBvcnRlZCwgZXhhbXBsZTogWydBbmRyb2lkJywgJ2lPUyddXG59KVxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIElvbmljU01TUmV0cmlldmVyIGV4dGVuZHMgSW9uaWNOYXRpdmVQbHVnaW4ge1xuXG5cdC8qKlxuXHQgKiBTdGFydCB0aGUgU01TIGxpc3RlbmVyXG5cdCAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIHRoZSBTTVNSZXRyaWV2ZXIgc3RhcnRzIGxpc3RlbmluZyB3aXRoIHN1Y2Nlc3MuXG5cdCAqL1xuXHRAQ29yZG92YSh7ICBjYWxsYmFja09yZGVyOiAncmV2ZXJzZScgfSlcblx0c3RhcnQoKTogUHJvbWlzZTxhbnk+IHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHQvKipcblx0ICogVGhpcyBmdW5jdGlvbiBkb2VzIHNvbWV0aGluZ1xuXHQgKiBAcGFyYW0gYXJnMSB7c3RyaW5nfSBTb21lIHBhcmFtIHRvIGNvbmZpZ3VyZSBzb21ldGhpbmdcblx0ICogQHBhcmFtIGFyZzIge251bWJlcn0gQW5vdGhlciBwYXJhbSB0byBjb25maWd1cmUgc29tZXRoaW5nXG5cdCAqIEByZXR1cm4ge1Byb21pc2U8YW55Pn0gUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIHNvbWV0aGluZyBoYXBwZW5zXG5cdCAqL1xuXHRAQ29yZG92YSh7ICBjYWxsYmFja09yZGVyOiAncmV2ZXJzZScgfSlcblx0dGVzdGUoYXJnMTogbnVtYmVyLCBhcmcyOiBhbnkpOiBQcm9taXNlPGFueT4ge1xuXHRcdHJldHVybjtcblx0fVxuXG59XG4iXX0=